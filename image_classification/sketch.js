let mobilenet;
let img;
const resultOutput = document.getElementById('result');
const state = document.getElementById('state');
let url = 'images/puffin.jpg';

function modelReady() {
  state.innerText = 'Model is ready!';
  state.className = 'ready';
  mobilenet.predict(img, gotResults);
}

function gotResults(error, results) {
  if (error) {
    console.error(error);
  } else {
    let currentResult = '';
    let i = 0;
    for (let result of results) {
      i++;
      currentResult = `${currentResult}<td>${i}. ${getPercent(result.probability)}: ${result.className}</td>`;
    }
    resultOutput.innerHTML = currentResult;
  }
}

function imageReady() {
  image(img, 0, 0, width, height);
}

function setup() {
  state.innerText = 'Model is not ready!';
  state.className = 'warning';
  resultOutput.innerHTML = '<td>Results are incoming...</td>';
  createCanvas(640, 480);
  img = createImg(url, imageReady);
  img.hide();
  background(0);
  mobilenet = ml5.imageClassifier('MobileNet', modelReady);
}

function getPercent(i) {
  return (i * 100).toFixed(0) + '%';
}

function uploadImage(event) {
  if (event) {
    url = URL.createObjectURL(event.target.files[0]);
    setup();
  }
}
