let mobilenet;
let video;
let entries = [];
let save = false;
const table = document.getElementById('table');
const current = document.getElementById('current');
const body = document.getElementById('body');


function modelReady() {
  state.innerText = 'Model is ready! Mouse click to save current result.';
  state.className = 'ready';
  mobilenet.predict(gotResults);
  body.addEventListener('click', () => {
    save = true;
  });
}

function gotResults(error, results) {
  if (error) {
    console.error(error);
  } else {
    if (save) {
      writeToTable(results);
    }
    let currentResult = '';
    let i = 0;
    for (let result of results) {
      i++;
      currentResult = `${currentResult}<td>${i}. ${getPercent(result.probability)}: ${result.className}</td>`;
    }
    current.innerHTML = currentResult;
    mobilenet.predict(gotResults);
    save = false;
  }
}

function writeToTable(result) {
  entries.unshift(result);
  let list = '';
  for (let entry of entries) {
    let size = entry.length > 3 ? 3 : entry.length;
    list = list + '<tr>';
    for (let i = 0; i <= size; i++) {
      if (entry[i]) {
        list = `${list}<td>${getPercent(entry[i]?.probability)}</td><td>${entry[i]?.className}</td>`;
      }
    }
    list = list + '</tr>';
  }
  table.innerHTML = list;
}

function setup() {
  state.innerText = 'Model is not ready!';
  state.className = 'warning';
  current.innerHTML = '<td>Results are incoming...</td>';
  createCanvas(640, 550);
  video = createCapture(VIDEO);
  video.hide();
  background(0);
  mobilenet = ml5.imageClassifier('MobileNet', video, modelReady);
}

function draw() {
  background(0);
  image(video, 0, 0);
  fill(255);
}

function getPercent(i) {
  return (i * 100).toFixed(0) + '%';
}
